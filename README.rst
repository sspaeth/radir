RADIR - The Radio Directory
===========================

vtuner & co offer nice services, but are proprietary and do not allow
toexport the raw data. Radir is supposed to be more friendly. The
backend code is under the GNU GPL, the stream data itself is under
the XXX.

Requirements
------------
python3, django (1.8 confirmed to work, 1.7 untested)
python3-djangorestframework (pip3 install restframework)


Deployment
----------
- Populate the database & create an admin user
  python3 manage.py migrate
  python3 manage.py createsuperuser

- Retrieve all the required static files before deploying:
  python3 manage.py collectstatic

