# This file is part of radir.
#
# radir is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# radir is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with radir.  If not, see
# <http://www.gnu.org/licenses/>.

from django.core.urlresolvers import reverse
from django.db import models
from django.template.defaultfilters import slugify

class Country(models.Model):
    name = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

class Language(models.Model):
    name = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Tag(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class StreamFormat(models.Model):
    name = models.CharField(max_length=40, unique=True)
    extension = models.CharField(max_length=3, unique=True)
    description = models.TextField(blank=True,null=True)

    def __str__(self):
        return self.name


class Stream(models.Model):
    url = models.URLField(null=True, blank=True, unique=True,
                          help_text="URL of the media stream.")
    format = models.ForeignKey(StreamFormat, on_delete=models.SET_NULL,
                               blank=True,
                               null=True,)
    bitrate = models.PositiveIntegerField(null=True, blank=True,
                  help_text="""Bitrate of the media stream (leave empty if """
                            """unknown).""")
    station = models.ForeignKey("Station", related_name='streams',
                                on_delete=models.CASCADE)
    last_checked = models.DateField(null=True, blank=True, default=None)

    def get_absolute_url(self):
        return reverse('stream-home', args=[self.station.pk, self.pk])

    def __str__(self):
        """Construct a nice string representation for our stream"""
        #TODO: Perhaps caching this string would be opportune for perf reasons.
        s = ""
        if self.format: s = str(self.format)
        if self.bitrate: s+= "-{}".format(self.bitrate)
        if s is not "":
            s += " ({})".format(self.station.slug)
            return s
        else: return self.station.slug

class Station(models.Model):
    name = models.CharField(max_length=40, unique=True,
               help_text="""Proper name of the radio station""")
    slug = models.SlugField(editable=True, unique=True)
    date_added = models.DateField(auto_now_add=True)
    likes = models.PositiveIntegerField(default=0)
    logo = models.ImageField(null=True, blank=True)
    country = models.ForeignKey(Country, on_delete=models.SET_NULL,
                                blank=True,
                                null=True,
        help_text="""Select the Country the station is in.""")
    language = models.ForeignKey(Language, on_delete=models.SET_NULL,
                                 blank=True,
                                 null=True,)
    tags = models.ManyToManyField(Tag, blank=True)
    website = models.URLField(null=True, blank=True,
                              help_text="Website of the radio station")
    #"stream_set" is reverse defined on a Stream.

    def save(self, *args, **kwargs):
        if not self.id or not self.slug:
            # Newly created object or no slug value, so set it
            self.slug = slugify(self.name)
        super(Station, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('station-home', args=[self.pk])

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
