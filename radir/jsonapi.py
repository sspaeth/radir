# This file is part of radir.
#
# radir is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# radir is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with radir.  If not, see
# <http://www.gnu.org/licenses/>.

from django.db.models import Count
from rest_framework import routers, serializers, viewsets
from radir.models import Country, Language, Tag, Station, Stream

# Serializers define the API representation.
class CountrySummarizedSerializer(serializers.HyperlinkedModelSerializer):
    """Provide a list of countries and overall stationcount per country"""
    stationcount = serializers.SerializerMethodField()
    def get_stationcount(self, obj):
        return obj.station_set.count()
    class Meta:
        model = Country
        fields = ('id', 'name', 'stationcount')

class CountryDetailedSerializer(serializers.HyperlinkedModelSerializer):
    """Retrieve a country with its full station list"""
    class Meta:
        model = Country
        fields = ('id', 'name', 'station_set')


class LanguageSummarizedSerializer(serializers.HyperlinkedModelSerializer):
    stationcount = serializers.SerializerMethodField()
    def get_stationcount(self, obj):
        return obj.station_set.count()
    class Meta:
        model = Language
        fields = ('id', 'name', 'stationcount')

class LanguageDetailedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Language
        fields = ('id', 'name', 'station_set')


class TagSummarizedSerializer(serializers.HyperlinkedModelSerializer):
    stationcount = serializers.SerializerMethodField()
    def get_stationcount(self, obj):
        return obj.station_set.count()
    class Meta:
        model = Tag
        fields = ('id', 'name', 'stationcount')

class TagDetailedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'station_set')


class StationSummarizedSerializer(serializers.HyperlinkedModelSerializer):
    country = serializers.CharField(read_only=True)
    language = serializers.CharField(read_only=True)
    tags = serializers.StringRelatedField(
        many=True,
        read_only=True)

    class Meta:
        model = Station
        fields = ('id','name', 'likes', 'country', 'language','tags')


class StationDetailedSerializer(serializers.HyperlinkedModelSerializer):
    country = serializers.CharField(read_only=True)
    language = serializers.CharField(read_only=True)
    tags = serializers.StringRelatedField(
        many=True,
        read_only=True)
    streams = serializers.HyperlinkedRelatedField(
        view_name="stream-detail",
        many=True,
        read_only=True)

    class Meta:
        model = Station
        fields = ('id','name', 'likes', 'country', 'language', 'website','tags','streams')

class StreamSerializer(serializers.ModelSerializer):
    station = serializers.HyperlinkedRelatedField(read_only=True,
                                                  view_name='station-detail')
    format = serializers.CharField(read_only=True)

    class Meta:
        model = Stream


# ViewSets define the view behavior.
class CountryViewSet(viewsets.ReadOnlyModelViewSet):
    """/api/v1/country/* API views for all country-based views.

    By default (/api/v1/country/) we only list all (used) countries
    together with a general station count for each country.

    Only in case of specific country calls (/api/v1/country/91), do we
    include the full list of stations in the reply. The lookup key, 91
    in the above case, is the country id.

    Use something like curl -H 'Accept: application/json; indent=4'
    http://XXX/api/v1/country/ to retrieve pure json.

    """
    queryset = Country.objects.annotate(
        Count('station')).filter(station__count__gt=0)
    serializer_class = CountrySummarizedSerializer
    #permission_classes = [IsAccountAdminOrReadOnly]

    def retrieve(self, request, pk=None):
        """Display full station set in case of .../country/1/"""
        try:
            self.queryset = Country.objects.filter(pk=pk)
        except ValueError:
            self.queryset = Country.objects.none()
        self.serializer_class = CountryDetailedSerializer
        return super(CountryViewSet, self).retrieve(request)


class LanguageViewSet(viewsets.ReadOnlyModelViewSet):
    """/api/v1/language/* API views for all language-based views.

    By default (/api/v1/language/) we only list all (used) language
    together with a general station count for each language.

    Only in case of specific calls (/api/v1/language/91), do we
    include the full list of stations in the reply. The lookup key, 91
    in the above case, is the language id.

    Use something like curl -H 'Accept: application/json; indent=4'
    http://XXX/api/v1/language/ to retrieve pure json.
    """
    queryset = Language.objects.annotate(
        Count('station')).filter(station__count__gt=0)
    serializer_class = LanguageSummarizedSerializer

    def retrieve(self, request, pk=None):
        try:
            self.queryset = Language.objects.filter(pk=pk)
        except ValueError:
            self.queryset = Language.objects.none()
        self.serializer_class = LanguageDetailedSerializer
        return super(LanguageViewSet, self).retrieve(request)


class TagViewSet(viewsets.ReadOnlyModelViewSet):
    """/api/v1/tag/* API views for all tag-based views.

    By default (/api/v1/tag/) we only list all (used) tag together with
    a general station count for each tag. A radio station might be
    associated with several tags.

    Only in case of specific calls (/api/v1/tag/91), do we
    include the full list of stations in the reply. The lookup key, 91
    in the above case, is the tag id.

    Use something like curl -H 'Accept: application/json; indent=4'
    http://XXX/api/v1/tag/ to retrieve pure json.
    """
    queryset = Tag.objects.annotate(
        Count('station')).filter(station__count__gt=0)
    serializer_class = TagSummarizedSerializer

    def retrieve(self, request, pk=None):
        try:
            self.queryset = Tag.objects.filter(pk=pk)
        except ValueError:
            self.queryset = Tag.objects.none()
        self.serializer_class = TagDetailedSerializer
        return super(TagViewSet, self).retrieve(request)


class StationViewSet(viewsets.ReadOnlyModelViewSet):
    """/api/v1/station/* API views for all station-based views.

    By default (/api/v1/station/) we only list a subset of information
    per radio station.

    Only in case of specific calls (/api/v1/station/91), do we include
    the full information about a specific station, for instance the list
    of associated media streams in the reply. The lookup key, 91 in the
    above case, is the station id.

    Use something like curl -H 'Accept: application/json; indent=4'
    http://XXX/api/v1/station/ to retrieve pure json.
    """
    queryset = Station.objects.all()
    serializer_class = StationSummarizedSerializer

    def retrieve(self, request, pk=None):
        """Display full station set in case of .../country/1/"""
        self.serializer_class = StationDetailedSerializer
        return super(StationViewSet, self).retrieve(request)


class StreamViewSet(viewsets.ReadOnlyModelViewSet):
    """/api/v1/stream/* API views for all stream-based views.

    The default call (/api/v1/stream/) will only return an empty
    response as this is a useless performance and bandwidth killer.

    Use specific calls (/api/v1/stream/91) for full information about a
    specific media stream. The lookup key, 91 in the above case, is the
    stream id as retrieved by the corresponding station lookup.

    Use something like curl -H 'Accept: application/json; indent=4'
    http://XXX/api/v1/stream/1/ to retrieve pure json.
    """
    queryset = Stream.objects.all()
    serializer_class = StreamSerializer

    def list(self, request):
        """Display empty set for the full monty"""
        self.queryset = Stream.objects.none()
        return super(StreamViewSet, self).list(request)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'country', CountryViewSet)
router.register(r'language', LanguageViewSet)
router.register(r'tag', TagViewSet)
router.register(r'station', StationViewSet)
router.register(r'stream', StreamViewSet)
