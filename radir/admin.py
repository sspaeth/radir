# This file is part of radir.
#
# radir is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# radir is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with radir.  If not, see
# <http://www.gnu.org/licenses/>.

from django.contrib import admin

from .models import *

class StationAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(Stream)
admin.site.register(Station, StationAdmin)
admin.site.register(Tag)
admin.site.register(StreamFormat)
admin.site.register(Country)
admin.site.register(Language)
