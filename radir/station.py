# This file is part of radir.
#
# radir is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# radir is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with radir.  If not, see
# <http://www.gnu.org/licenses/>.

from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.shortcuts import get_object_or_404
from django.views.generic import (CreateView, DetailView, ListView, UpdateView,
                                  RedirectView)

from radir.models import Station, Stream, Country, Language, Tag


class CountryListView(ListView):
    model    = Country
    context_object_name = 'country_list'
    queryset = Country.objects.annotate(Count('station')).filter(
               station__count__gt=0)
    paginate_by = 20
    paginate_orphans = 5

class LanguageListView(ListView):
    model    = Language
    context_object_name = 'language_list'
    queryset = Language.objects.annotate(Count('station')).filter(
               station__count__gt=0)
    paginate_by = 20
    paginate_orphans = 5


class TagListView(ListView):
    model = Tag
    context_object_name = 'tag_list'
    queryset = Tag.objects.annotate(Count('station')).filter(
               station__count__gt=0)
    paginate_by = 20
    paginate_orphans = 5


class StationListView(ListView):
    model = Station
    context_object_name = 'station_list'
    paginate_by = 20
    paginate_orphans = 5

    def get_queryset(self):
        stations = Station.objects.all()

        # Are we filtering by country?
        self.country = self.kwargs.get('country')
        if self.country:
            self.country = get_object_or_404(Country,
                                         name=self.country)
            stations = stations.filter(country=self.country)

        # Are we filtering by language?
        self.lang = self.kwargs.get('language')
        if self.lang:
            self.lang = get_object_or_404(Language, name=self.lang)
            stations = stations.filter(language=self.lang)

        # Are we filtering by tag?
        self.tag = self.kwargs.get('tag')
        if self.tag:
            self.tag = get_object_or_404(Tag, name=self.tag)
            stations = stations.filter(tags=self.tag)

        return stations

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(StationListView, self).get_context_data(
            **kwargs)
        # Add in our possible filter criteria
        context['country'] = self.country
        context['language'] = self.lang
        context['tag'] = self.tag
        return context

    def get_template_names(self):
        if self.country is not None:
            template_name = "radir/station_list_countries.html"
        elif self.lang is not None:
            template_name = "radir/station_list_lang.html"
        elif self.tag is not None:
            template_name = "radir/station_list_tag.html"
        else:
            template_name = super(StationListView, self).get_template_names()
        return template_name


class StationDetailView(DetailView):
    model = Station
    context_object_name = 'station'

class StationCreateView(CreateView):
    model = Station
    fields = ['name', 'logo', 'country', 'language', 'tags', 'website']

class StationUpdateView(UpdateView):
    model = Station
    fields = ['name', 'logo', 'country', 'language', 'tags', 'website']
    context_object_name = 'station'


class StreamView(RedirectView):
    """Redirects the browser to the media URL of a station."""
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        stream_id = self.kwargs.get('pk',None)
        if stream_id:
            return get_object_or_404(Stream, pk=stream_id).url

        # If no specific stream pk was given, load the default stream of
        # this station (aka the first in our list).
        station = get_object_or_404(Station,
                                         pk=self.kwargs['station'])
        url = Stream.objects.filter(station=station).first().url
        return url
        #TODO: Implement "FORMAT" kwarg handling


class StreamCreateView(CreateView):
    """Add a new Stream to an existing station."""
    model = Stream
    fields = '__all__' #['url', 'format', 'bitrate']

    def get_success_url(self):
        """ Return to the stations detail page after adding a stream."""
        return reverse('station-home', args=[self.kwargs.get('station')])

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(StreamCreateView, self).get_context_data(
            **kwargs)
        # Add in our possible filter criteria
        self.station = get_object_or_404(Station,
                                         pk = self.kwargs.get('station'))
        context['station'] = self.station
        return context

    def get_form(self):
        """ Override get_form to set the initial station value ourselves """
        form = super(StreamCreateView, self).get_form()
        form.fields['station'].initial = self.kwargs.get('station')
        return form


class StreamUpdateView(UpdateView):
    """Edit an existing stream."""
    model = Stream
    fields = ['url', 'format', 'bitrate']
    context_object_name = 'stream'

    def get_success_url(self):
        """ Return to the stations detail page after editing a stream."""
        return reverse('station-home', args=[self.kwargs.get('station')])

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(StreamUpdateView, self).get_context_data(
            **kwargs)
        # Add in our possible filter criteria
        self.station = get_object_or_404(Station,
                                         pk = self.kwargs.get('station'))
        context['station'] = self.station
        return context
