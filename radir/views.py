# This file is part of radir.
#
# radir is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# radir is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with radir.  If not, see
# <http://www.gnu.org/licenses/>.

from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db.models import Count
from django.views.generic import TemplateView, RedirectView, UpdateView
from django.views.generic.edit import CreateView
from radir.models import Station, Country, Language, Tag

class RootView(TemplateView):
    template_name = "root.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(RootView, self).get_context_data(
            **kwargs)
        # Add in summary statistics for the homepage
        context['num_countries'] = Country.objects.annotate(
            Count('station')).filter(station__count__gt=0).count()
        context['num_languages'] = Language.objects.annotate(
            Count('station')).filter(station__count__gt=0).count()
        context['num_tags'] = Tag.objects.annotate(
            Count('station')).filter(station__count__gt=0).count()
        context['num_stations'] = Station.objects.count()
        return context


class LicenseView(TemplateView):
    template_name = "licenses.html"

class UserProfileRedirectView(UpdateView):
    """Show the user's profile page"""
    model = User
    #form_class = UserChangeForm
    fields = ['first_name','last_name','email']
    def get_success_url(self): return reverse('user_profile')

    def get_object(self, queryset=None):
        return self.request.user

class UserCreateView(CreateView):
    #TODO: Log the user automatically in after creating her
    template_name='registration/register.html'
    form_class=UserCreationForm,
    success_url='/accounts/profile/'

class UserUpdateView(UpdateView):
    model = User
    form_class = UserChangeForm
    exclude = ['password']
    #    fields = ['__all__']

