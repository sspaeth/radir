# This file is part of radir.
#
# radir is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# radir is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with radir.  If not, see
# <http://www.gnu.org/licenses/>.

"""radir URL Configuration"""
from django.db.models import Count
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from radir.jsonapi import router
from radir.station import (StationCreateView, StationDetailView,
                           StationUpdateView, StationListView,
                           CountryListView, LanguageListView, TagListView,
                           StreamCreateView, StreamView, StreamUpdateView)
from radir.views import (RootView,LicenseView, UserProfileRedirectView,
                         UserCreateView, UserUpdateView)

urlpatterns = [
    # Admin and Account urls:
    url(r'^admin/', include(admin.site.urls)),
    url('^accounts/', include('django.contrib.auth.urls')),
    url('^accounts/register/$', UserCreateView.as_view(), name="register"),
    url('^accounts/profile/$', login_required(
        UserProfileRedirectView.as_view()), name="user_profile"),
    #url('^accounts/id/(?P<pk>[\d]+)$', UserUpdateView.as_view(),
    #    name="user_profile"),
    url(r'^$', RootView.as_view(), name="root"),
    url(r'^licenses/$', LicenseView.as_view(), name="licenses"),
    url(r'^country/$', CountryListView.as_view(),
        name='country-listing'),

    url(r'^language/$', LanguageListView.as_view(),
        name='language-listing'),
    url(r'^language/(?P<language>[\w-]+)/$', StationListView.as_view(),
        name='station-by-language'),

    url(r'^tag/$', TagListView.as_view(),
        name='tag-listing'),
    url(r'^tag/(?P<tag>[\w-]+)$', StationListView.as_view(),
        name='station-by-tag'),

    # Station Views
    url(r'^station/$', StationListView.as_view(),
        name='station-listing'),
    url(r'^country/(?P<country>[\w --,.]+)$', StationListView.as_view(),
        name='station-by-country'),
    url(r'^station/(?P<pk>[\d]+)/$', StationDetailView.as_view(),
        name='station-home'),
    url(r'^station/add/$', StationCreateView.as_view(),
        name='station-create'),
    url(r'^station/(?P<pk>[\d]+)/edit/$', StationUpdateView.as_view(),
        name='station-edit'),

    # Stream Views
    # Request a specific stream format, e.g. OGG
    url(r'^station/(?P<station>[\d]+)/stream/format/((?P<format>[\w-]+)/)?$',
        StreamView.as_view(),
        name='stream-format'),
    url(r'^station/(?P<station>[\d]+)/stream/add/$', StreamCreateView.as_view(),
        name='stream-create'),
    url(r'^station/(?P<station>[\d]+)/stream/(?P<pk>[\d]+)/edit/$',
        StreamUpdateView.as_view(),
        name='stream-edit'),
    # Load the "default" stream (aka the first in our list?!)
    url(r'^station/(?P<station>[\d]+)/stream/$',
        StreamView.as_view(),
        name='stream-home'),
    # Load a specific stream
    url(r'^station/(?P<station>[\d]+)/stream/(?P<pk>[\d]+)/$',
        StreamView.as_view(),
        name='stream-home'),

    url(r'^api/v1/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))

]
