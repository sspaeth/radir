# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=40)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=40)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Station',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=40)),
                ('slug', models.SlugField(unique=True)),
                ('date_added', models.DateField(auto_now_add=True)),
                ('likes', models.PositiveIntegerField(default=0)),
                ('logo', models.ImageField(null=True, blank=True, upload_to='')),
                ('website', models.URLField(null=True, blank=True, help_text='Website of the radio station')),
                ('country', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.SET_NULL, null=True, to='radir.Country')),
                ('language', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.SET_NULL, null=True, to='radir.Language')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Stream',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('url', models.URLField(null=True, blank=True, help_text='Website of the radio station', unique=True)),
                ('bitrate', models.PositiveIntegerField(blank=True, null=True)),
                ('last_checked', models.DateField(default=None, blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='StreamFormat',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=40)),
                ('extension', models.CharField(unique=True, max_length=3)),
                ('description', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=50)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.AddField(
            model_name='stream',
            name='format',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.SET_NULL, null=True, to='radir.StreamFormat'),
        ),
        migrations.AddField(
            model_name='stream',
            name='station',
            field=models.ForeignKey(to='radir.Station', related_name='streams'),
        ),
        migrations.AddField(
            model_name='station',
            name='tags',
            field=models.ManyToManyField(to='radir.Tag'),
        ),
    ]
